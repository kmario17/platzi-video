import React from 'react';
import Media from './media';
import './playlist.css';
import {Play,Pause,FullScreen,Volumen} from '../../icons/components/figure';

function Playlist(props)  {
        //const data = props.data.categories;
        return(
            <div className="Playlist">
                {
                  props.playlist.map((item)=> {
                     return <Media key={item.id} {...item}/>
                  })
                }  
            </div>  
        )
}

export default Playlist;