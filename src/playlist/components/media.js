import React,{Component} from 'react';
import './media.css';
import PropTypes from "prop-types";

class Media extends Component {
    constructor(props){
        super(props)
        this.state = {
            author: props.author
        }
    }
    componentDidMount(){
        console.log('componentDidMount')
    }
    handleClick = (event) => {
        console.log(event)
        this.setState({
            author: 'juan'
        })
    }
    
    render(){
        console.log('render')
        return(
           <div className="Media" onClick={this.handleClick} >
              <div className="Media-cover"> 
                 <img 
                    src={this.props.cover}
                    alt={this.props.title}
                    width={260}
                    height={160}
                    className="Media-image"
                />
                <h3 className="Media-title"> {this.props.title} </h3>
                <p className="Media-author">{this.props.author}</p>
               </div>
          </div>
        )
    }
}

Media.propTypes = {
   cover: PropTypes.string, 
   title: PropTypes.string,
   author: PropTypes.string,
   type: PropTypes.oneOf(['video','audio'])
}

export default Media