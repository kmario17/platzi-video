import React,{Component} from 'react';

function HomeLayout(props) {
    return(
        <section>
           {props.children}
        </section>
    )
}

export default HomeLayout;